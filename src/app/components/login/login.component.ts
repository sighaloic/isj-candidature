import { Component, NgZone, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SocialAuthService, FacebookLoginProvider, GoogleLoginProvider, SocialUser } from 'angularx-social-login';
import { CrudUserService } from 'src/app/service/crud-user.service';
import { LoginService } from 'src/app/service/login.service';
import { User } from 'src/app/service/User';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  socialUser: SocialUser = new SocialUser;
  isSignedin: boolean = false;
  loginForm: any;
  isSubmittedLogin = false;
  user: User = new User;
  registerForm: FormGroup;
  isSubmittedRegister = false;
  

  constructor(
    private formBuilder: FormBuilder,
    private socialAuthService: SocialAuthService,
    private loginService : LoginService,
    private ngZone : NgZone,
    private router: Router,
    private crudUserService: CrudUserService
  ) { 
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword: ['', [Validators.required]],
      role: ['', [Validators.required]],
      status: ['A']

    }, {
      validator: MustMatch('password', 'confirmPassword')
  });
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required, Validators.email],
      password: ['', Validators.required]
    });  

    this.socialAuthService.authState.subscribe((user) => {
      this.socialUser = user;
      this.isSignedin = (user != null);
      console.log('User from Social' + this.user);
    });
  }

  facebookSignin(): void {
    this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then((data) => {
      localStorage.setItem('facebook_auth', JSON.stringify(data));
      this.router.navigateByUrl('/dashboard').then();
    });
  }

  loginWithGoogle(): void {
    this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then((data) => {
      localStorage.setItem('google_auth', JSON.stringify(data));
      this.router.navigateByUrl('/dashboard').then();
    });
  }

   logOut(): void {
     this.socialAuthService.signOut();
   }

  onSubmitLogin(): any{

    this.isSubmittedLogin = true;
    if(this.loginForm.invalid){
      return;
    }else{
      this.loginService.SignIn(this.loginForm.value)
      .subscribe(() => {
        console.log(JSON.stringify(this.loginForm.value))
        this.ngZone.run(() => this.router.navigateByUrl('/dashboard'))
      }, (err) =>{
        console.log(err);
      });
    }
  }


// Register
facebookSignup(): void {
  this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID).then((data) => {

    this.user.firstName = data.firstName;
    this.user.lastName = data.lastName;
    this.user.name = data.name;
    this.user.email = 'data.email';
    this.user.photoUrl = data.photoUrl;
    this.user.accessToken = data.authToken;
    this.user.status = 'A';
    this.user.role = 'candidat';
    this.user.password = 'facebook';

    this.crudUserService.AddUser(this.user)
    .subscribe(() => {
      console.log('Data added successfully! (' + JSON.stringify(this.user) + ')')
      this.ngZone.run(() => this.router.navigateByUrl('sign-in'))
    }, (err) =>{
      console.log(err);
    });

    // localStorage.setItem('facebook_auth', JSON.stringify(data));
    // this.router.navigateByUrl('/dashboard').then();
  });
}

signupWithGoogle(): void {
  this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID).then((data) => {

    this.user.firstName = data.firstName;
    this.user.lastName = data.lastName;
    this.user.name = data.name;
    this.user.email = data.email;
    this.user.photoUrl = data.photoUrl;
    this.user.accessToken = data.authToken;
    this.user.status = 'A';
    this.user.role = 'candidat';
    this.user.password = 'google';

    this.crudUserService.AddUser(this.user)
    .subscribe(() => {
      console.log('Data added successfully! (' + JSON.stringify(this.user) + ')')
      this.ngZone.run(() => this.router.navigateByUrl('sign-in'))
    }, (err) =>{
      console.log(err);
    });
  });
}

onSubmitRegister(): any{
  this.isSubmittedRegister = true;
  if(this.registerForm.invalid){
    return;
  }else{
    this.crudUserService.AddUser(this.registerForm.value)
    .subscribe(() => {
      console.log('Data added successfully! (' + JSON.stringify(this.registerForm.value) + ')')
      this.ngZone.run(() => this.router.navigateByUrl('add-user'))
    }, (err) =>{
      console.log(err);
    });
  }
}

}

function MustMatch(controlName: string, matchingControlName: string) {
  return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
          // return if another validator has already found an error on the matchingControl
          return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
          matchingControl.setErrors({ mustMatch: true });
      } else {
          matchingControl.setErrors(null);
      }
  }
}
