import { Component, OnInit } from '@angular/core';
import { CrudUserService } from 'src/app/service/crud-user.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {

  Users:any = [];

  constructor(private crudUserService: CrudUserService) { }

  ngOnInit(): void {
    this.crudUserService.GetUsers().subscribe(res => {
      console.log(res)
      this.Users =res;
    });    
  }

  delete(id:any, i:any) {
    console.log(id);
    if(window.confirm('Do you want to go ahead?')) {
      this.crudUserService.deleteUser(id).subscribe((res) => {
        this.Users.splice(i, 1);
      })
    }
  }
}
