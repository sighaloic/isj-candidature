import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public userDetails: any;
  appComponent!: AppComponent;

  constructor(
    private router : Router
  ) { }

  
  ngOnInit(): void {

    let storage = null;

    if(localStorage.getItem('native_auth')){
      storage = localStorage.getItem('native_auth');
    }
    if(localStorage.getItem('google_auth')){
      storage = localStorage.getItem('google_auth');
    }
    if(localStorage.getItem('facebook_auth')){
      storage = localStorage.getItem('facebook_auth');
    }

    

    if(storage){
      this.userDetails = JSON.parse(storage);
    }else{
      this.appComponent.logOut();
    }
  }
}
