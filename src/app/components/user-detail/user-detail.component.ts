import { Component, NgZone, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { CrudUserService } from 'src/app/service/crud-user.service';
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  getId : any;
  updateForm : FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    private router: Router,
    private ngZone: NgZone,
    private activatedRoute: ActivatedRoute,
    private crudUserService: CrudUserService
  ) {

    this.getId = this.activatedRoute.snapshot.paramMap.get('id');

    this.crudUserService.GetUser(this.getId).subscribe(res => {
      this.updateForm.setValue({
        email: res['email'],
        password: res['password']
      });
    });

    this.updateForm = this.formBuilder.group({
      email: [''],
      password: ['']
    })

   }

  ngOnInit(): void {
  }

  onUpdate(): any {
    this.crudUserService.updateUser(this.getId, this.updateForm.value)
    .subscribe(() => {
        console.log('Data updated successfully!')
        this.ngZone.run(() => this.router.navigateByUrl('/edit-user/'+ this.getId))
      }, (err) => {
        console.log(err);
    });
  }

}
