import { Injectable, NgZone } from '@angular/core';
import { User } from './User';
import { catchError, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  // Node/Express API
  REST_API: string = 'http://localhost:3001/api';

  // Http Header
  httpHeaders = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private ngZone: NgZone
    ) { }

  //Add
  SignIn(data: User): Observable<any>{
    let API_URL = `${this.REST_API}/login`;
    localStorage.setItem('native_auth', JSON.stringify(data));
    this.ngZone.run(() => this.router.navigateByUrl('/dashboard'));
    console.log(data);
    return this.httpClient.post(API_URL, data)
    .pipe(
      catchError(this.handleError)
    )
  }

  //Add
  ResSignIn(): Observable<any>{
    let data = new User();
    localStorage.setItem('_auth', JSON.stringify(data));
    return this.httpClient.get(`${this.REST_API}`);
  }

  // Error 
  handleError(error: HttpErrorResponse) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Handle client error
      errorMessage = error.error.message;
    } else {
      // Handle server error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
