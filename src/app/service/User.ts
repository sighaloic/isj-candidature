export class User {
    firstName!: String;
    lastName!: String;
    name!: String;
    email!: String;
    password!: String;
    role!: String;
    accessToken!: String;
    status!: String;
    photoUrl!: String
}