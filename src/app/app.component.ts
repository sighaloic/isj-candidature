import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { SocialAuthService, FacebookLoginProvider, GoogleLoginProvider, SocialUser } from 'angularx-social-login';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ISJ-CANDIDATURE';
  isLoggedIn = false;

    

  constructor(
    private socialAuthService: SocialAuthService,
    private router: Router
  ) { }

  logOut(): void {
    this.socialAuthService.signOut();
    localStorage.removeItem('google_auth');
    localStorage.removeItem('facebook_auth');
    localStorage.removeItem('native_auth',);
    this.router.navigateByUrl('/login').then();
  }

  ngOnInit(): void {
    
    if(localStorage.getItem('native_auth') || localStorage.getItem('google_auth') || localStorage.getItem('facebook_auth')){
      this.isLoggedIn = true;
    }
  }

  
}
