import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserDetailComponent } from './components/user-detail/user-detail.component';
import { UsersListComponent } from './components/users-list/users-list.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { TestComponent } from './components/test/test.component';
import { LoginComponent } from './components/login/login.component';


const routes: Routes = [
  { path: "", pathMatch: 'full', redirectTo: 'sign-in'},
  { path: 'users-list', component: UsersListComponent },
  { path: 'edit-user/:id', component: UserDetailComponent },
  { path: 'home', component: UsersListComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'test', component: TestComponent },
  { path: 'login', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
